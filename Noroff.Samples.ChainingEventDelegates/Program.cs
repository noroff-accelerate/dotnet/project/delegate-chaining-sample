﻿namespace Noroff.Samples.ChainingEventDelegates
{
    // Simple data model for an Order
    public class Order
    {
        public string Id { get; set; }
        public string ProductId { get; set; }
    }

    public delegate void OrderProcessHandler(Order order);

    /// <summary>
    /// Main business logic class with all the steps needed for ordering.
    /// If new steps are needed, it is simple to add it to the chain of events with +=
    /// </summary>
    public class OrderProcessing
    {
        // The event keyword allows other classes to "listen" for this ProcessOrder and extend the event handling - all without exposing the actual delegate, reducing coupling.
        // We can technically omit the "event" keyword and the application still functions, but its worth seeing it here, so you learn to recognise it later.
        public event OrderProcessHandler ProcessOrder;

        public OrderProcessing()
        {
            // Attaching static methods from different classes to the ProcessOrder delegate
            // This is essentially the processing pipeline.
            ProcessOrder += PaymentValidator.ValidatePayment;
            ProcessOrder += InventoryManager.UpdateInventory;
            ProcessOrder += EmailService.SendConfirmationEmail;
            ProcessOrder += ShippingService.InitiateShipping;
        }

        public void OnOrderPlaced(Order order)
        {
            // Invoking the delegate, which will call all attached methods in sequence
            ProcessOrder?.Invoke(order);
        }
    }

    // All the implementations for the various funcitonalities that are chained together.

    public static class PaymentValidator
    {
        public static void ValidatePayment(Order order)
        {
            Console.WriteLine("Payment validated for order: " + order.Id);
        }
    }

    public static class InventoryManager
    {
        public static void UpdateInventory(Order order)
        {
            Console.WriteLine("Inventory updated for product: " + order.ProductId);
        }
    }

    public static class EmailService
    {
        public static void SendConfirmationEmail(Order order)
        {
            Console.WriteLine("Confirmation email sent to customer for order: " + order.Id);
        }
    }

    public static class ShippingService
    {
        public static void InitiateShipping(Order order)
        {
            Console.WriteLine("Shipping initiated for order: " + order.Id);
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            // Instantiating the OrderProcessing class
            OrderProcessing orderProcessing = new OrderProcessing();

            // Creating a new order
            Order newOrder = new Order { Id = "1234", ProductId = "5678" };

            // Triggering the order processing - only one line needed to call all the methods.
            orderProcessing.OnOrderPlaced(newOrder);
        }
    }

}